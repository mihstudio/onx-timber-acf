export interface IFrontBoxConfig {
	name: string
	files: string | string[]
	dest: string
	watch: string | string[]
	concatWith?: string
	otherTasksImpact?: boolean
}

export interface IFrontBoxTask {
	destinationPath?: string
	canConcatFiles?: boolean
}
