import { BrowserService } from './services/browser/browser.service';
import  stickyHeader  from "../components/header/header";
import  articleslider  from "../components/arcticle-slider/arcticle-slider";
import  benefitslider  from "../components/benefit-slider/benefit-slider";
import  cards  from "../components/cards/cards";
import  {career}  from "../components/career/career";
import  clientsSlider  from "../components/clients/clients";
import  wordShifter  from "../components/word-shifter/word-shifter";
import  UkSwitcherMinHeight  from "../components/it-progress/it-progress";
import  csscreen  from "../components/cs-screen/cs-screen";
import  counter  from "../components/counter/counter";
import { rating }  from "../components/rating/rating";
import  teamslider   from "../components/team/team";
import  cstech  from "../components/cs-tech/cs-tech";
import lazyVideo from "../components/banner/banner";
import blogHero from "../components/blog/hero/blog-hero";
import sharePost from "../components/share-box/share-box";
import blogPost from "../views/blog-post/blog-post";


export const browserService = new BrowserService()

const UIkit = require('uikit');
const Slick = require('slick-carousel/slick/slick');

window.addEventListener("DOMContentLoaded", () => {

    csscreen();
    stickyHeader();
    articleslider();
    benefitslider();
    counter();
    cards();
    career();
    clientsSlider();
    wordShifter();
    UkSwitcherMinHeight();
    rating();
    teamslider();
    cstech();
    lazyVideo();
    blogHero();
    sharePost();
    blogPost();

}, { once: true });
