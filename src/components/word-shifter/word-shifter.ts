const $ = require('jquery');

/* functions */
function wordShifter(){

  const wordShifterEl = $('.word-shifter');

  wordShifterEl.each(function(){
    let i = 0
    const children = $(this).find('.word-shifter__item');

    children.eq(0).addClass('word-shifter__item--active');

    if(children.length > 1 ){
      setInterval(function(){
        children.removeClass('word-shifter__item--active');
        children.eq(i).removeClass('word-shifter__item--init');
        children.eq(i).addClass('word-shifter__item--active');
        i++;
        if (i >= children.length ){i = 0;}
      }, 8000);
    }
  });
}

export default wordShifter;
