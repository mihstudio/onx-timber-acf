import { InputType } from "zlib";

import {getElement, getElements} from '../../scripts/tools/DOM';

export const copyLink = () => {
    if(!getElement('#copy-link-js')) return;
    let link: HTMLLinkElement = getElement('#copy-link-js');
    let text = link.href;
    link.addEventListener('click', copyStringToClipboard, false);

    function copyStringToClipboard(e: Event) {
        e.preventDefault();

        let el: any = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style = {position: 'absolute', left: '-9999px'};
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }
}