<?php
/**
 * WordPress gutenberg blocks
 *
 * @package Onex
 */

/**
 * Register custom ACF blocks
 */
function register_acf_block_types() {
	acf_register_block_type(
		array(
			'name'            => 'cta-large',
			'title'           => __( 'CTA large' ),
			'description'     => __( 'A custom block for CTA large.' ),
			'render_template' => 'src/components/acf-blocks/cta-large.twig',
			'render_callback' => 'acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="48" height="48" role="img" aria-hidden="true" focusable="false"><path d="M4 17h7V6H4v11zm9-10v1.5h7V7h-7zm0 5.5h7V11h-7v1.5zm0 4h7V15h-7v1.5z"></path></svg>',
			'keywords'        => array( 'cta, commercial, large' ),
		)
	);

	acf_register_block_type(
		array(
			'name'            => 'cta-medium',
			'title'           => __( 'CTA medium' ),
			'description'     => __( 'A custom block for CTA medium.' ),
			'render_template' => 'src/components/acf-blocks/cta-medium.twig',
			'render_callback' => 'acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="48" height="48" role="img" aria-hidden="true" focusable="false"><path d="M4 17h7V6H4v11zm9-10v1.5h7V7h-7zm0 5.5h7V11h-7v1.5zm0 4h7V15h-7v1.5z"></path></svg>',
			'keywords'        => array( 'cta, commercial, medium' ),
		)
	);
}

if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'register_acf_block_types' );
	add_action( 'enqueue_block_editor_assets', 'add_block_editor_assets', 10, 0 );
}

/**
 * Add block editor styles
 */
function add_block_editor_assets() {
	wp_enqueue_style( 'block_editor_css', get_template_directory_uri() . '/public/theme.css', array(), '1', 'all' );
}

/**
 * Render callback for registered ACF blocks
 *
 * @param string $block Custom block object.
 */
function acf_block_render_callback( $block ) {
	$vars['block']  = $block;
	$vars['fields'] = get_fields();
	Timber::render( $block['render_template'], $vars );
}
