const $ = require('jquery');


/* functions */
function sharePost() {

    if (($(".share--copy").length > 0)) {

        $('.share--copy').on({
            "click": function (e) {
                e.preventDefault();
                var copyText = $(this).data('url');

                document.addEventListener('copy', function (e) {
                    e.clipboardData.setData('text/plain', copyText);
                    e.preventDefault();
                }, true);

                document.execCommand('copy');
                $(this).children('.copy-info').addClass('active');
                setTimeout(function() {
                    $('.copy-info.active').removeClass('active');
                }, 2000);
            },
        });
    }
}

export default sharePost;