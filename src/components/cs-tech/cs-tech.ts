const $ = require('jquery');


/* functions */
function csTech() {

  const sliderEl = $('.cs-tech__set-inner');

  sliderEl.each(function() {

    $(this).slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: false,
      autoplay: true,
      draggable: false,

      responsive: [
        {
          breakpoint: 5280,
          settings: "unslick"
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]

    });

  });
}

export default csTech;
