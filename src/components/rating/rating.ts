const $ = require('jquery');

export function rating() {

  const ratingEl = $('.js-rating__total[data-rating-total]');

  if (ratingEl.length) {

    ratingEl.each(function() {

        const score = parseFloat($(this).data("rating-total"));
        const starsFull = Math.floor(score);
        const starsRest = score - Math.floor(score);
        let i = 0;

        $(this).append( score );

        for ( i = 0; i < starsFull; i++ ){
          $(this).append('<span class="rating__total-star">');
        }

        if (starsRest <= 0.33){
          $(this).append('<span class="rating__total-star rating__total-star--025">');
        }else if (starsRest <= 0.66) {
          $(this).append('<span class="rating__total-star rating__total-star--05">');
        }else if (starsRest < 1) {
          $(this).append('<span class="rating__total-star rating__total-star--075">');
        }

        for ( i; i < 4; i++ ){
          $(this).append('<span class="rating__total-star rating__total-star--0">');
        }


      });

  }
}
