const $ = require('jquery');
const Slick = require('slick-carousel/slick/slick');

(function($) {

  cards();
  cardsMobile();

  $( window ).on('resize', function(){
    cards();
    cardsMobile();
  });

})($);



/* functions */
function cards() {

  const cards = $('.cards__item:not(.cards__item--active)');
  const cardsSet = $('.cards__set');

  cardsSet.on('click', '.cards__item:not(.cards__item--active)', function(){
    if($(window).width() > 640){
      $(this).addClass("cards__item--becoming-nr-one");
      let parent = $(this).closest('.cards__set');
      let moveMe = $(this);

      $('.cards__item--active').removeClass('cards__item--active');

      setTimeout(function(){
        parent.prepend(moveMe);

      }, 400);

      setTimeout(function(){
        moveMe.addClass('cards__item--active');
        moveMe.removeClass("cards__item--becoming-nr-one");
      }, 800);

      cards.unbind( "click" );
      // cards();
    }
  });
}



function cardsMobile(){
  if($(window).width() <= 640){
    $('.cards__set').slick({
      infinite: false,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true
    });
  }
}

export default cards;