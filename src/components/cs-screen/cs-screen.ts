const $ = require('jquery');

/* functions */
function csscreen(){

    const csScreens = $('.js-cs-screen');
    let i = 0;
  
    csScreens.each(function(){
      const websiteHeight  = $(this).find('.js-cs-screen__screen img').outerHeight();
      const websitewidth   = $(this).find('.js-cs-screen__screen img').outerWidth();
      const screenheight   = $(this).find('.js-cs-screen__screen').outerHeight();
      const move           = websiteHeight - screenheight;
      let animationCode    = "";
  
      $(this).find('.js-cs-screen__screen img').addClass('js-csroll-anim-' + i); 
  
       animationCode += "<style>";
  
       animationCode += ".js-csroll-anim-" + i + " { ";
       animationCode += "   animation: scroll-" + i + " 8s infinite ease-in-out;";
       animationCode += "}";
  
       animationCode += "@keyframes scroll-" + i + " { ";
       animationCode += "   0%{ transform: translateY(0) }";
       animationCode += "   20%{ transform: translateY(0) }";
       animationCode += "   40%{ transform: translateY(-" + move/3*1 + "px)}";
       animationCode += "   60%{ transform: translateY(-" + move/3*2 + "px)}";
       animationCode += "   80%{ transform: translateY(-" + move + "px) }";
       animationCode += "   90%{ transform: translateY(-" + move + "px) }";
       animationCode += "   100%{ transform: translateY(0px) }}";
  
       animationCode += "</style>";
  
       $(this).append(animationCode);
  
       i =+ 1;
  
    });
  
  
  
  }

export default csscreen;
  