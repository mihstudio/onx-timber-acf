const $ = require('jquery');

/* functions */

function counter(){

  const counterNumber = $('[data-counter-target]');

  counterNumber.each(function(){

    const that = $(this);

    that.on('inview.uk.scrollspy', function(){

      $({countNum: that.text()}).animate({countNum: that.data('counter-target')}, {
        duration: 4000,
        easing:'linear',
        step: function() {
          that.text(Math.floor(this.countNum));
        },
        complete: function() {
          that.text(this.countNum);
        }
      });
    });

  });
}

export default counter;
