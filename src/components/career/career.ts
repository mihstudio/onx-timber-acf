const $ = require('jquery');

/* functions */
export function career(){

  function screenaAnim(){
    const csScreens = $('.js-career__screen');
    const websiteHeight  = $('.js-career__screen img').outerHeight();
    const screenHeight   = csScreens.outerHeight();
    const move           = websiteHeight - screenHeight;
    let animationCode    = "";
    let i = 0;

    csScreens.find('img').addClass('js-career-csroll');

     animationCode += "<style>";

     animationCode += ".js-career-csroll { ";
     animationCode += "   animation: career-scroll 8s infinite ease-in-out;";
     animationCode += "}";

     animationCode += "@keyframes career-scroll { ";
     animationCode += "   0%{ transform: translateY(0) }";
     animationCode += "   20%{ transform: translateY(0) }";
     animationCode += "   40%{ transform: translateY(-" + move/3*1 + "px)}";
     animationCode += "   60%{ transform: translateY(-" + move/3*2 + "px)}";
     animationCode += "   80%{ transform: translateY(-" + move + "px) }";
     animationCode += "   90%{ transform: translateY(-" + move + "px) }";
     animationCode += "   100%{ transform: translateY(0px) }}";

     animationCode += "</style>";

     csScreens.append(animationCode);
  }
  screenaAnim();


  function openSlide(){
    const sectionEl = $('.js-career');
    const indicatorEl = $('.js-indicator');

    indicatorEl.on('click', function(){
      sectionEl.toggleClass('career--active');
      indicatorEl.toggleClass('indicator--active');

      if( $(this).hasClass('indicator--active') ){
        setTimeout(function(){
          indicatorEl.toggleClass('career__indicator--position');
        }, 500);
      }else{
        indicatorEl.toggleClass('career__indicator--position');
      }
    });
  }
  openSlide();
}