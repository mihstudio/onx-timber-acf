const $ = require('jquery');

/* functions */
function clientsSlider(){

  const parent = $('.js-clients__slider');
  const inner = $('.js-clients__inner');
  const sliderEl = $('.js-clients__slider .clients__inner');
  const children = $('.js-clients__slider .clients__item');

  if (parent.outerWidth() > inner.outerWidth()){
    parent.addClass('clients__slider--disabled');
  }else{
    let style = "";
    let width = 0;

    children.each(function(){
      width = width + $(this).outerWidth();
    });

    sliderEl.css('width', width + 'px');

    let move =  width - parent.outerWidth();

    style = "<style>@keyframes clientsSlider { 0%{ transform: translate3d(0, 0, 0); } 100%{ transform: translate3d(-" + move  + "px, 0, 0);}}</style>";

    sliderEl.css('animation', 'clientsSlider ' + move / 25 + 's linear 0s infinite alternate none running ');

    sliderEl.append(style)
  }

}

export default clientsSlider;
