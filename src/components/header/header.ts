const $ = require('jquery');

function stickyHeader() {

    const header = $('header.header');
    $(document).ready(function () {
        let scroll = $(window).scrollTop();

        if (scroll > 0) {
            header.addClass('header--sticky')
        } else {
            header.removeClass('header--sticky')
        }

        $(window).on('scroll resize', function () {
            let scroll = $(this).scrollTop();
            if (scroll > 0) {
                header.addClass('header--sticky')
            } else {
                header.removeClass('header--sticky')
            }
        });

    });

    $('.main-nav .main-nav__item > .arrow-bottom').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next('.nav-drop').toggleClass('open-menu');
    });

    $('.nav-drop .nav-drop-item > .arrow-bottom').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next('.nav-drop').toggleClass('open-menu');
    });
}

export default stickyHeader;