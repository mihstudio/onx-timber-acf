const $ = require('jquery');


/* functions */
function blogHero(){

  const sliderEl = $('.hero-blog');

  sliderEl.each(function(){

    const that = $(this);

    $(this).slick({
      dots: true,
      arrows: false,
      centerMode: false,
      slidesToShow: 1,
      slidesToscroll: 1,
      infinite: true,
      initialSlide: 0,
    });

    $(this).on('click', '.slick-slide:not(.slick-current)', function(){
        that.slick('slickGoTo', parseInt($(this).data('slick-index')) );
    });
  });
}

export default blogHero;
