import { getElements } from '../../scripts/tools/DOM';
import * as LazyLoad from "lazyload";

export const imageInit = (element?: HTMLElement) => {
  const lazyImagesEl = getElements('[data-src]:not([src])', element || null);

  if (!lazyImagesEl.length) {
    return
  }

  new LazyLoad(lazyImagesEl);
}
