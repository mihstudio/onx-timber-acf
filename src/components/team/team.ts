const $ = require('jquery');


/* functions */
function teamslider(){

  const sliderEl = $('.about-team__init');

  sliderEl.each(function(){

    const that = $(this);

    $(this).slick({
      dots: false,
      centerMode: true,
      slidesToShow: 1,
      slidesToscroll: 1,
      infinite: false,
      initialSlide: 2,
      responsive: [
      {
        breakpoint: 640,
        settings: {
          dots: true,
          arrows: false,
          initialSlide: 0
        }
      }
    ]

    });

    $(this).on('click', '.slick-slide:not(.slick-current)', function(){
        that.slick('slickGoTo', parseInt($(this).data('slick-index')) );
    });
  });
}

export default teamslider;
