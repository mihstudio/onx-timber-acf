<?php
/**
 * WordPress Timber Extension
 *
 * @package Onex
 */

/**
 * Extend functions
 */
class ListSummaryBlockAcf {
	/**
	 * Constructor
	 */
	public function __construct() {
		$params = array(
			'name'            => 'list-summary',
			'title'           => 'Listy z podsumowaniem',
			'category'        => 'text',
			'render_callback' => array( $this, 'render' ),
		);

		acf_register_block_type( $params );
	}

	/**
	 * Render template.
	 */
	public function render() {
		$fields = get_fields();
		$items  = $fields['items'];

		$params = array(
			'items' => $items,
		);

		Timber::render( 'list-summary/list-summary.twig', $params );
	}
}

new ListSummaryBlockAcf();
