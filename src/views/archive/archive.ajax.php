<?php
/**
 *  Category Load Post AJAX
 *
 * @package Onex
 */

/**
 * Init request of posts
 */
function load_more_posts() {
}

add_action( 'wp_ajax_load_more_posts', 'load_more_posts' );
add_action( 'wp_ajax_nopriv_load_more_posts', 'load_more_posts' );
