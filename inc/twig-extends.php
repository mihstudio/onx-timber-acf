<?php
/**
 * Twig extends
 *
 * @package Onex
 */

add_filter(
	'timber/twig',
	function ( $twig ) {
		$twig->addExtension(
			new Twig_Extension_StringLoader()
		);

		$twig->addFilter(
			new Twig_SimpleFilter(
				'image_url',
				function ( $text ) {
					return get_template_directory_uri() . '/static/images/' . $text;
				}
			)
		);

		$twig->addFilter(
			new Twig_SimpleFilter(
				'icon_url',
				function ( $text ) {
					return get_template_directory_uri() . '/static/icons/' . $text . '.svg';
				}
			)
		);

		$twig->addFilter(
			new Twig_SimpleFilter(
				'date_format',
				function ( $date ) {
					$now        = new DateTime();
					$d          = new DateTime( $date );
					$difference = $now->diff( $d );
					switch ( $difference->d ) {
						case 0:
							return 'Today';
						case 1:
							return '1 day ago';
						case ( $difference->d <= 7 ):
							return $difference->d . ' days ago';
						default:
							return $date;
					}
				}
			)
		);
		$twig->addFilter(
			new Twig_SimpleFilter(
				'wysiwyg',
				function ( $value ) {
					return apply_filters( 'the_content', $value );
				}
			)
		);

		$twig->addFilter(
			new Twig_SimpleFilter(
				'icon',
				function ( $icon_name, $class_names = false ) {
					$filename = get_template_directory() . '/static/icons/' . $icon_name . '.svg';

					if ( file_exists( $filename ) ) {
						$icon = file_get_contents( $filename );

						if ( $class_names ) {
							$icon = str_replace( '<svg', "<svg class='icon " . $class_names . "'", $icon );
						}

						return $icon;
					} else {
						return "<span style='color:#ff0000; font-size: 14px;'>!$icon_name'</span>";
					}
				}
			)
		);

		/**
		 * Get headers tag from content.
		 */
		$twig->addFunction(
			new Timber\Twig_Function(
				'get_headers',
				function ( $content ) {
					preg_match_all( '/<h(2).*?>(.*)<\/h2+>/', $content, $matches );

					if ( empty( $matches ) ) {
						return;
					}

					$output = array();
					$tags   = $matches[1];
					$titles = $matches[2];

					foreach ( $tags as $key => $tag ) {
						$title = wp_strip_all_tags( $titles[ $key ] );

						switch ( $tag ) {
							case 2:
								$output[] = array(
									'title' => $title,
									'index' => $key,
									'child' => array(),
								);

								break;
						}
					}

					return $output;
				}
			)
		);

		return $twig;
	}
);

add_filter(
	'timber_context',
	function ( $context ) {
		global $site;
		$options_default = get_fields( 'options_default' );

		if ( class_exists( 'ACF' ) ) {
			$context['options'] = get_fields( 'options' );
		}

		$context['cookies'] = $_COOKIE;
		$context['site']    = $site;

		return $context;
	}
);

/**
 * Get date time
 *
 * @param string $post_date Post date.
 */
function date_time_ago( $post_date ) {
	global $post;
	$post_date = strtotime( $post_date );
	return human_time_diff( $post_date, current_time( 'timestamp' ) ) . ' ' . __( 'temu' );
}


/**
 * Get proper word based on ordinal number
 *
 * @param string $number Number of items.
 */
function proper_word( $items_count ) {
	$last_number = substr( $items_count, -1 );
	if ( $items_count == 1 ) {
		echo $items_count . ' artykuł';
	} elseif ( ( $items_count > 1 and $items_count < 5 ) and ( $last_number == 2 or $last_number == 4 ) ) {
		echo $items_count . ' artykuły';
	} else {
		echo $items_count . ' artykułów';
	}
}
